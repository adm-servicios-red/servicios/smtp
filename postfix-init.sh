#!/usr/bin/env bash

PID="/var/spool/postfix/pid/master.pid"

if [ -f "$PID" ]; then
  rm -v $PID
else
  mkdir -v -p /var/spool/postfix/pid
fi
touch /var/log/postfix.log
postfix -c /etc/postfix/ start && tail -f /var/log/postfix.log
