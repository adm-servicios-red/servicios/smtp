FROM ubuntu:16.04

LABEL maintainer="César Pérez Canseco (Lvnar)"
SHELL ["/bin/bash", "-c"]

ENV DEBIAN_FRONTEND=noninteractive
ENV DOMAIN=lvnar.com


RUN dpkg --configure -a && \
    apt-get update -q --fix-missing && \
    apt-get -y upgrade && \
    apt-get install -y \
    apt-utils \
    dnsutils \
    postfix \
    dovecot-common \
    dovecot-imapd \
    dovecot-pop3d \
    #dovecot-postfix \
    vim

COPY ./main.cf /etc/postfix/main.cf
COPY ./check-for-changes.sh ./postfix-init.sh /usr/local/bin/
RUN chmod +x /usr/local/bin/*
RUN test -f /etc/aliases || mkdir -p /etc/aliases
RUN newaliases

CMD ./usr/local/bin/postfix-init.sh
